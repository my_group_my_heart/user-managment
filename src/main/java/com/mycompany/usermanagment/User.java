/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.usermanagment;

/**
 *
 * @author 66986
 */
public class User {
    private String userName;
    private String passwword;

    public User(String userName, String passwword) {
        this.userName = userName;
        this.passwword = passwword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPasswword() {
        return passwword;
    }

    public void setPasswword(String passwword) {
        this.passwword = passwword;
    }

    @Override
    public String toString() {
        return "User{" + "userName=" + userName + ", passwword=" + passwword + '}';
    }
    
}
