/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.usermanagment;

import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


/**
 *
 * @author 66986
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    
    static {
    
    userList.add(new User("admin", "password"));
    userList.add(new User("user1", "password"));
    }
    //Create
    public static boolean addUser(String userName, String password){
        userList.add(new User(userName,password));
        return true;
    }
     public static boolean addUser(User user){
        userList.add(user);
        return true;   
     }
     public static boolean uodateUser(int index,User user){
         userList.set(index,user);
         return true;
     }
     public static User getUser(int index){
         if(index>userList.size()-1){
             return null;
     }
            return userList.get(index);
     }
     public static ArrayList<User> getUsers(){
         return userList;
     }
     public static ArrayList<User> searchUserName(String searchText){
         ArrayList<User> list = new ArrayList<>();
         for(User user: userList){
             if(user.getUserName().startsWith(searchText)){
                 list.add(user);
             }
         }
         return list;
     }
    public static boolean delUser(int index){
        userList.remove(index);
        return true;
    }
     public static boolean delUser(User user){
        userList.remove(user);
        return true;
     }
     public static User Login(String userName,String password){
         for(User user: userList){
             if(user.getUserName().equals(userName)&&user.getPasswword().equals(password)){
                 return user;
             }
         }
         return null;
     }
     public static void save() {
        FileOutputStream fos = null;
        try {
            File file = new File("User.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(UserService.getUsers());
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                fos.close();
            } catch (IOException ex) {

            }
        }
    }

     public static void load() {
        FileInputStream fis = null;
        try {
            File file = new File("User.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<User> temp = (ArrayList<User>) ois.readObject();
            UserService.addUser(temp);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } catch (ClassNotFoundException ex) {

        } finally {
            try {
                fis.close();
            } catch (IOException ex) {

            }
        }
    }


private static void addUser(ArrayList<User> temp) {
        userList.addAll(temp);
    }
}

    
            
    
            
